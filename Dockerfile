FROM drupal:8.3.2-apache

# needed for https only apt repositories, like percona
RUN apt-get update \
    && apt-get install -y apt-transport-https ca-certificates \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

### Add Percona repository to be able to add mysql client
### Source: https://github.com/docker-library/percona/blob/master/5.7/Dockerfile

ENV GPG_KEYS \
# pub   1024D/CD2EFD2A 2009-12-15
#       Key fingerprint = 430B DF5C 56E7 C94E 848E  E60C 1C4C BDCD CD2E FD2A
# uid                  Percona MySQL Development Team <mysql-dev@percona.com>
# sub   2048g/2D607DAF 2009-12-15
	430BDF5C56E7C94E848EE60C1C4CBDCDCD2EFD2A \
# pub   4096R/8507EFA5 2016-06-30
#       Key fingerprint = 4D1B B29D 63D9 8E42 2B21  13B1 9334 A25F 8507 EFA5
# uid                  Percona MySQL Development Team (Packaging key) <mysql-dev@percona.com>
# sub   4096R/4CAC6D72 2016-06-30
	4D1BB29D63D98E422B2113B19334A25F8507EFA5
RUN set -ex; \
	export GNUPGHOME="$(mktemp -d)"; \
	for key in $GPG_KEYS; do \
		gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
	done; \
	gpg --export $GPG_KEYS > /etc/apt/trusted.gpg.d/percona.gpg; \
	rm -r "$GNUPGHOME"; \
	apt-key list; \
	echo 'deb https://repo.percona.com/apt jessie main' > /etc/apt/sources.list.d/percona.list

ENV PERCONA_MAJOR 5.7

### End of Percona repository configuration

RUN curl -sL https://deb.nodesource.com/setup_7.x | bash - \
    && apt-get install -y --no-install-recommends python-software-properties wget nodejs \
    && apt-get install -y percona-server-client-$PERCONA_MAJOR \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer \
    && apt-get install -y git \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

RUN curl https://drupalconsole.com/installer -L -o drupal.phar \
    && mv drupal.phar /usr/local/bin/drupal \
    && chmod +x /usr/local/bin/drupal

RUN echo 'export PATH="/root/.composer/vendor/bin:$PATH"' > /root/.bashrc \
    && composer global require drush/drush

ARG DEBUG

RUN if [ "$DEBUG" = "1" ]; then \
    pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo 'xdebug.remote_connect_back=0' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo 'xdebug.remote_log=/var/log/remoteconnect.log' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && touch /var/log/remoteconnect.log && chmod 777 /var/log/remoteconnect.log; \
    fi
